Rails.application.routes.draw do
 
  resources :participations
  resources :locations
  resources :tournaments
  resources :sessions, :only => [:new, :create, :destroy]
  resources :users
  resources :rec_password, :only => [:new, :create]

  root 'tournaments#index'

  match '/signup', to: 'users#new', via:'get'
  match '/signin', to: 'sessions#new', via:'get'
  match '/signout', to: 'sessions#destroy', via: 'delete'
  match '/verify', to: 'users#accountVerification', via: 'get'
  match '/pass', to: 'rec_password#new', via: 'get'
  match '/reset', to: 'rec_password#reset', via: 'get'
  match '/resetAction', to: 'rec_password#resetAction', via: 'post'
  match '/sponsor', to: 'tournaments#sponsor', via: 'post'
  match '/sponsor/:id', to: 'tournaments#sponsorDelete', via: 'delete'
  match '/join', to: 'tournaments#new_join', via: 'get'
  match '/join', to: 'tournaments#create_join', via: 'post'
  match '/participation/:id', to: 'tournaments#leftTournament', via: 'delete'
  match '/my_tournaments', to: 'tournaments#my', via: 'get'
  match '/start', to: 'tournaments#start', via: 'get'
  match '/matches', to: 'tournaments#showMatches', via: 'get' 
  match '/nextRound', to: 'tournaments#nextRound', via: 'post'
end
