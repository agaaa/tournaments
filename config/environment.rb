# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!

# Using will_paginate plugin
#config.gem 'will_paginate', :version => '~> 2.3.15'
