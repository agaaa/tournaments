class CreateParticipations < ActiveRecord::Migration
  def change
    create_table :participations do |t|
      t.integer :user_id
      t.integer :tournament_id
      t.integer :license
      t.integer :ranking

      t.timestamps
    end
  end
end
