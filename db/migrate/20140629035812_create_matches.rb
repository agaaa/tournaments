class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
	  t.integer :playerA
	  t.integer :playerB
	  t.integer :winnerA
	  t.integer :winnerB
	  t.integer :participation_id
	  t.integer :round_id
      t.timestamps
    end
  end
end
