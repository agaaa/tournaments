class CreateTournamentSponsors < ActiveRecord::Migration
  def change
    create_table :tournament_sponsors do |t|
      t.integer :tournament_id
      t.integer :sponsor_id

      t.timestamps
    end
  end
end
