class CreateTournaments < ActiveRecord::Migration
  def change
    create_table :tournaments do |t|
      t.string :name
      t.integer :location_id
      t.datetime :time
      t.datetime :deadline
      t.integer :limit
      t.integer :seeded_player
      t.integer :promoter_id

      t.timestamps
    end
  end
end
