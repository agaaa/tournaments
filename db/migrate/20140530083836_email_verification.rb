class EmailVerification < ActiveRecord::Migration
  def change
	add_column :users, :verification_token, :string
	add_column :users, :verification_email_sent_at, :datetime
	add_column :users, :verified_at, :datetime
  end
end
