#Tutaj dzieje się cała magia związana z logowaniem, czyli pamiętanie danych usera w sesji, usuwanie ich itd.

module SessionsHelper
	#Funkcja tworzy nowy obiekt użytkownika w sesji, zapisuje ciasteczka itd.
	def sign_in(user)
		#Patrz plik models/user.rb
		remember_token=User.new_remember_token
		cookies.permanent[:remember_token]=remember_token
		user.update_attribute(:remember_token, User.digest(remember_token))
		##
		#To jest metoda, nie zmienna, ta notacja ruby'ego ;d
		self.current_user=user
	end

	#Funkcja usuwa obiekt usera z sesji oraz ciasteczka, czyli wszystko co potrzebna przy wylogowywaniu się
	def sign_out
		current_user.update_attribute(:remember_token,User.digest(User.new_remember_token))
		cookies.delete(:remember_token)
		self.current_user = nil
  	end

	#To jest bardzo przydate ;d Funkcja sprawdza, czy user jest zalogowany. Dzięki temu możemy np. ukrywać treść dla niezalogowanych userów. Przykład jest w pliku views/layouts/_header.html.erb
	def signed_in?
    	!current_user.nil?
 	end

	def current_user=(user)
    	@current_user = user
 	end

	def current_user
    	remember_token = User.digest(cookies[:remember_token])
    	@current_user ||= User.find_by(remember_token: remember_token)
  	end
end
