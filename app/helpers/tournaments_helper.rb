module TournamentsHelper
	def generateFirstRound(t_id)
		round=Round.new
		round.tournament_id=t_id
		round.phase=1
		round.save
		participants=Participation.where(:tournament_id => t_id).order(:ranking)
		seeded=(1<<(Math.log2(participants.size).to_i+1))-participants.size
		#flash[:notify]=seeded
		i=0; j=seeded+participants.size-1
		while(i<j) do
			if i<seeded
				match=Match.new
				match.playerA=participants[i].user_id
				match.round_id=round.id
				match.save	
			else
				match=Match.new
				match.playerA=participants[i].user_id
				match.playerB=participants[j].user_id
				match.round_id=round.id
				match.save
			end
			i+=1
			j-=1
		end
	end

	def generateNextRound(t_id)
		r=Round.where(:tournament_id => t_id).order(phase: :desc).first
		if !r.nil?	
			matches=Match.where(:round_id=>r.id).size
			winners=Match.where("participation_id IS NOT NULL")
			winners=winners.where(:round_id => r.id).joins(:participation).order("participations.ranking")
			if winners.size == matches
				round=Round.new
				round.tournament_id=t_id
				round.phase=r.phase+1
				round.save
				i=0; j=winners.size-1
				while(i<j) do
					match=Match.new
					match.playerA=winners[i].participation_id
					match.playerB=winners[j].participation_id
					match.round_id=round.id
					match.save
					i+=1
					j-=1
				end
			end	
		end	
	end
end
