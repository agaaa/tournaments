class VerifyMailer < ActionMailer::Base
	default :from => "tournamentapp2014@gmail.com"	
	
	#Wyślij maila związanego z aktywacją konta
	def send_email(user)
		@user = user
		@url="http://localhost:3000/verify?user_id="+user.verification_token
		mail(to: user.email, subject: 'Welcome to TournamentApp').deliver()
	end

	#Wyślij maila związanego z resetowaniem hasła
	def send_password(email,user)
		@user=user
		@email=email
		@url="http://localhost:3000/reset?reset_id="+user.verification_token
		mail(to: email, subject: 'Password recovery').deliver()
	end
end
