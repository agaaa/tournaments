json.array!(@participations) do |participation|
  json.extract! participation, :id, :user_id, :tournament_id, :license, :ranking
  json.url participation_url(participation, format: :json)
end
