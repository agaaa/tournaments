class RecPasswordController < ApplicationController
	
	##Ten kontroler obsługuje odzyskiwanie hasła		
	
	def new 
	end

	#Ta akcja wywoływana jest, gdy klikniemy na link w mailu 
	def reset
		#Trzeba było jakoś przekazac weryfikujący token między akcjami, dlatego umieściłem go w zmiennej sesji
		#Ten token do kolejna kolumna w bazie users. Wartość tokena dla danego usera jest doczepiana do linku resetującego, jako parametr. 
		session[:token]=params['reset_id']
		@user=User.find_by(verification_token: session[:token])
		if @user == nil || @user.verification_token==nil
			flash[:error]="Url not found" 
			redirect_to root_url
		end
	end

	#Resetujemy hasło, dla danego usera token jest ustawiany z powrotem na null	
	def resetAction
		@user=User.find_by(verification_token: session[:token])
		session[:token]=nil;
		if @user.update_attributes(user_params)
			@user.update_attribute(:verification_token, nil)
			flash[:notify]="Password has been changed"
			redirect_to root_url
		else
			render 'reset'
		end
	end

	#Wysyłamy maila i umieszczamy w bazie token
	def create
		user=User.find_by(email: params[:session][:username].downcase)
		email=params[:session][:email].downcase
		if user
			if email=~/\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
				user.update_attribute(:verification_token, User.new_remember_token)
				VerifyMailer.send_password(email,user)
				flash[:notify]="Check your email to reset your password"
				redirect_to root_url
			else
				flash.now[:error]="Incorrect email "
				render 'new'
			end
		else
			flash.now[:error]="Username not found"
			render 'new'
		end
	end
	
	private
	def user_params
    	params.require(:user).permit(:password, :password_confirmation)
    end


end
