class TournamentsController < ApplicationController
 
  def index
    @tournaments = Tournament.all.paginate(page: params[:page], per_page: 10)
  end

  def my
    @tournaments = Tournament.where(id: (Participation.where(user_id: current_user.id))).paginate(page: params[:page], per_page: 10)
  end

  def new
    if signed_in?	
      @tournament = Tournament.new
      @location = Location.new
    else
      flash[:error] = "You have to be signed in to create a new tournament!"
      redirect_to signin_path
    end
  end

  def new_join
    if signed_in? 
      if Participation.where(tournament_id: session[:id], user_id: current_user.id).count > 0
        flash[:error] = "You have already joined this tournament!"
        redirect_to Tournament.find(session[:id])
      else
        @participation = Participation.new
      end
    else
      flash[:error] = "You have to be signed in to join to the tournament!"
      redirect_to signin_path
    end
  end

  def create_join
    if Participation.where(tournament_id: session[:id]).count < Tournament.find(session[:id]).limit
      @participation = Participation.new 
      @participation.user_id = current_user.id
      @participation.tournament_id = session[:id]
      @participation.ranking = params[:participant][:ranking]
      @participation.license = params[:participant][:license]

      if @participation.save
        flash[:success] = "Joined tournament."
        redirect_to '/tournaments'+'/'+session[:id].to_s
      else
        respond_to do |format|
          format.html { render :new_join }
          format.json { render json: @participant.errors, status: :unprocessable_entity }
        end
      end
    else
      flash[:error] = "You cannot join tournament. Number of participants is full."
      redirect_to '/tournaments'+'/'+session[:id].to_s
    end
  end

  def leftTournament
    tournament=Tournament.find_by_id(session[:id])
    Participation.find_by_id(params[:id]).destroy
    redirect_to tournament
  end 

  def show 
    set_tournament
	generateNextRound(@tournament.id)
    @participations = Participation.all
    session[:id]=@tournament.id
    @spons=Sponsor.new
    @sponsors=TournamentSponsor.all
    @json = Location.find(@tournament.location_id).to_gmaps4rails
  end

  def edit
    if signed_in? 
      set_tournament
      if @tournament.deadline > Time.now + 2*60*60
        if @tournament.promoter_id != current_user.id
          flash[:error] = "You can edit only your tournaments!"
          redirect_to root_url
        end
      else
          flash[:error] = "You cannot edit tournament which deadline refers to the past!"
          redirect_to root_url
      end
    else
      flash[:error] = "You have to be signed in to edit your tournament!"
      redirect_to signin_path
    end
  end

  def create
      @tournament = Tournament.new(tournament_params)

      @location = Location.new
      @location.address = params[:tournament][:location_id]
      geocoder = Geocoder.search(@location.address)
      if(geocoder[0].nil?)
        @location.longitude = -1
        @location.latitude = -1
        @location.gmaps = false
      else
        @location.longitude = geocoder[0].longitude
        @location.latitude = geocoder[0].latitude
        @location.gmaps = true
      end

      if @location.save 
        location_saved = true
      else
        location_saved = false
      end

      if @tournament.save
        tournament_saved = true
      else
        tournament_saved = false
      end

      if tournament_saved and location_saved
        @tournament.update_attribute(:promoter_id, current_user.id)
        @tournament.update_attribute(:location_id, @location.id)
        flash[:success]="Tournament created"
        redirect_to root_url
      else
        if tournament_saved
          @tournament.delete
        end
        if location_saved = true
          @location.delete
        end

        respond_to do |format|
          format.html { render :new }
          format.json { render json: @tournament.errors, status: :unprocessable_entity }
        end
      end
  end

  def update
      set_tournament

      #location changed
      if @location.address != params[:tournament][:location_id]
        geocoder = Geocoder.search(params[:tournament][:location_id])
        if(!geocoder[0].nil?)
       	 @location.address = params[:tournament][:location_id]   
          @location.longitude = geocoder[0].longitude
          @location.latitude = geocoder[0].latitude
          @location.gmaps = true
        end
      end

      if @tournament.update(tournament_params) && @location.save
        flash[:success]="Tournament updated"
        @tournament.update_attribute(:location_id,@location.id)
        redirect_to root_url
      else
        @tournament.location_id=@location.id
        respond_to do |format|
          format.html { render :edit }
          format.json { render json: @tournament.errors, status: :unprocessable_entity }
        end
      end
  end

  def destroy
    set_tournament
    Location.find(@tournament.location_id).destroy
    @tournament.destroy
    redirect_to root_url
  end

  def sponsorDelete
    tournament=Tournament.find_by_id(session[:id])
    TournamentSponsor.find_by_id(params[:id]).destroy
    redirect_to tournament
  end

  def sponsor
    extension=nil
    @spons=Sponsor.find_by_name(params[:sponsor][:name])
    if @spons==nil
      @spons=Sponsor.new
      @spons.name=params[:sponsor][:name]
      if params[:sponsor][:img]!=nil
		tmp=params[:sponsor][:img].original_filename.split(".")
		if !tmp[1].nil?
        	extension=tmp[1]
		else
			extension="png"
		end
        @spons.logo="/images/"+@spons.name+"."+extension
      end
      if @spons.save
        tournamentSponsor=TournamentSponsor.new
        tournamentSponsor.tournament_id=session[:id]
        tournamentSponsor.sponsor_id=@spons.id
        tournamentSponsor.save
        if extension!=nil
          filesave(params[:sponsor][:img],@spons.name+"."+extension)
        end
		tournament=Tournament.find_by_id(session[:id])
    	redirect_to tournament
	  else
	  	@tournament=Tournament.find_by_id(session[:id])
		@participations = Participation.all
    	@sponsors=TournamentSponsor.all
		@user=current_user
    	@json = Location.find(@tournament.location_id).to_gmaps4rails
		render :show	
	  end
    else
      tournamentSponsor=TournamentSponsor.new
      tournamentSponsor.tournament_id=session[:id]
      tournamentSponsor.sponsor_id=@spons.id
      tournamentSponsor.save
      if extension!=nil
        filesave(params[:sponsor][:img],@spons.name+"."+extension)
      end
	  tournament=Tournament.find_by_id(session[:id])
      redirect_to tournament
    end
  end
	

  def start
	generateFirstRound(session[:id])
	redirect_to '/matches'
  end

  def showMatches
	current_round=Round.where(:tournament_id => session[:id]).order(phase: :desc).first	
	@matches=Match.where(:round_id=>current_round.id)
  end

  def nextRound
	winner=params[:round][:choose]
	flash[:notify]=winner
	current_round=Round.where(:tournament_id => session[:id]).order(phase: :desc).first	
	current_matches=Match.where(:round_id=>current_round.id)
	match=current_matches.find_by(:playerA=>current_user.id)
	#flash[:notify]=match.id
	#generateNextRound(session[:id],current_round)	
	if !match.nil? 
		if !match.winnerB.nil?
			if match.winnerB==winner.to_i
				match.update_attribute(:participation_id,winner)
				#generateNextRound(session[:id],current_round)
			else 
				match.update_attribute(:winnerB,nil)
			end
		else
			match.update_attribute(:winnerA,winner)
		end
	else
		match=current_matches.find_by(:playerB=>current_user.id)
		if !match.winnerA.nil?
			if match.winnerA==winner.to_i
				match.update_attribute(:participation_id,winner)
				#generateNextRound(session[:id],current_round)
			else 
				match.update_attribute(:winnerA,nil)
			end
		else
			match.update_attribute(:winnerB,winner)
		end
	end
	redirect_to '/matches'	
  end

  private

  def filesave(upload,name) 
    directory = "app/assets/images"
    path = File.join(directory, name)
    File.open(path, "wb") { |f| f.write(upload.read) }
  end

  def set_tournament
    if signed_in?
      @user=current_user
    end
    @tournament = Tournament.find(params[:id])
    @location = Location.find(@tournament.location_id)
  end

  def tournament_params
    params.require(:tournament).permit(:name, :promoter_id, :time, :limit, :deadline, :seeded_player,:location_id)
  end

end
