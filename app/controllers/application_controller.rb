class ApplicationController < ActionController::Base
  before_filter :set_no_cache  #prevent browser from caching 
  before_action :initTournaments
  protect_from_forgery with: :exception
  include SessionsHelper
  include TournamentsHelper

  def set_no_cache
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end


  def initTournaments
	Tournament.all.each do |t| 
		round=Round.find_by(:tournament_id=>t.id)	
		if Time.zone.now>t.time && round.nil?
			generateFirstRound(t.id)
		end	
	end	
  end

end
