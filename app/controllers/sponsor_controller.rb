class SponsorsController < ApplicationController

  
  def new
    @sponsor = Sponsor.new()
  end


  def create
    @sponsor = Sponsor.new(sponsor_params)
    if !@sponsor.save
      respond_to do |format|
        format.html { render :new }
        format.json { render json: @sponsor.errors, status: :unprocessable_entity }
      end
    end 
  end 

  def sponsor_params
    params.require(:sponsor).permit(:name, :logo)
  end

end
