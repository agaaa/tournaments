class UsersController < ApplicationController
	#Wyświetlamy widok  do rejestracji
  def new
    @user = User.new
  end

	#To wyświetla widok po zalogowaniu użytkownika
  def show
  end
	
  #funkcja przekazuje dane do formularza do edycji danych profilowych
  def edit
  	set_user
  end

  #To się wykonuje, gdy zatwierdzimy zmienione dane na formularzu
  def update
	set_user
    if @user.update_attributes(user_params)
	  #To przekazuje napis do ramki, że wszystko poszło ok
      flash[:success] = "Profile updated"
	  #Przekieruj nas do show.html.erb
      redirect_to @user
    else
		#Jak coś pójdzie żle to wyświetl znowu stronę do edycji
      render 'edit'
    end
  end
 
	#Ta funkcja jest wywoływana, gdy zatwierdzimy rejestrację 
  def create
    @user = User.new(user_params)
	if @user.save
		
		#Te pola są używane podczas weryfikacji maila
		@user.update_attribute(:verification_token, User.new_remember_token)
		@user.update_attribute(:verification_email_sent_at, Time.now)
		##
		#Wysyłamy maila
		VerifyMailer.send_email(@user)
		flash[:success]="Verifying e-mail has been sent"
		render 'show'	
	else
		render 'new'
	end
  end

  def destroy
	set_user
	sign_out
    @user.destroy
	flash[:success]="Profile deleted"
	redirect_to root_url
  end

  def accountVerification
	if(verifyEmail(params['user_id']))		  
		flash.now[:success]="Your account has been activated, you can sign in now"
	else
		flash[:error]="Url not found"
		redirect_to root_url
  	end
  end

  private
    
  	def set_user
      @user = User.find(params[:id])
    end

	def verifyEmail(param)
		user=User.find_by(verification_token: params['user_id'])
		if user && user.verification_token!=nil
			user.update_attribute(:verification_token, nil)
			user.update_attribute(:verification_email_sent_at, nil)
			user.update_attribute(:verified_at, Time.now)
			true
		else
			false
		end
	end	

    def user_params
      params.require(:user).permit(:name, :surname, :email, :password, :password_confirmation)
    end
end
