class SessionsController < ApplicationController
	def new
	end
	
	#Akcja wywoływana przy odzyskiwaniu hasła
	#Ta funkcja wykonuje się po kliknięciu na Sign in przy logowaniu
	def create
		user=User.find_by(email: params[:session][:email].downcase)
		if user && user.authenticate(params[:session][:password])
			#Sprawdzamy czy user potwiedził adres email
			if user.verified_at!=nil && user.verification_email_sent_at==nil
				#Ta funkcja zapisuje informacje o zalogowanym użytkowniku w sesji
				#Jest zadeklarowana w pliku helpers/sessions_helper.rb 
				#przekierowanie do show.html.erb
				user.update_attribute(:verification_token, nil) if user.verification_token !=nil 
				sign_in user
				redirect_to root_url	
			else
				flash[:error]="You need to confirm your email adress"
				redirect_to signin_path
			end
			#Ta funkcja zapisuje informacje o zalogowanym użytkowniku w sesji
			#Jest zadeklarowana w pliku helpers/sessions_helper.rb 
			#przekierowanie do show.html.erb 
		else
			#To wysyła napis do takiej ramki, która się pojawia przy błędnym logowaniu
			flash.now[:error] = 'Sorry, invalid email or password'
			#wczytujemy jeszcze raz stronę logowania
			render 'new'
		end
	end

	#To się wykonuje jak się wylogowujemy
	def destroy
		sign_out
		redirect_to root_url
	end
end
