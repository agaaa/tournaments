class Location < ActiveRecord::Base
  acts_as_gmappable
  validates :address, presence: true 
  belongs_to :tournament

  def gmaps4rails_address
    address
  end
end
