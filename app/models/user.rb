class User < ActiveRecord::Base

	#Tutaj te wszystkie zależności między tabelami itd.
    has_many :participant, dependent: :nullify #:destroy, :delete_all, :nullify, :restrict_with_error, :restrict_with_exception
    has_many :promoter
    has_one :participant
	
	##

	#przed zapisem w bazie, wszytskie litery w adresie email są zamieniane na małe
	before_save { self.email = email.downcase }
	
	#Tutaj zaczyna się walidacja danych przy rejestracji
	validates :name, presence: true, length: { maximum: 50 }
	
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence:   true,
				        format:     { with: VALID_EMAIL_REGEX },
					uniqueness: { case_sensitive: false }
	
	has_secure_password
	validates :password, length: { minimum: 6 }
	##

	#Te funkcje związane są z bezpieczeństwem po zalogowaniu, generują jakieś losowe ciągi znaków, które są zapisywane w ciasteczkach po zalogowaniu. Chodzi o to żeby nikt nie przejął sesji innego użytkownika po zalogowaniu. FunJeszcze do poczytania na ten temat ;)	
	def User.new_remember_token
    	SecureRandom.urlsafe_base64
  	end

  	def User.digest(token)
    	Digest::SHA1.hexdigest(token.to_s)
  	end
	##


  private
	def create_remember_token
    	self.remember_token = User.digest(User.new_remember_token)
    end
end
