class Participation < ActiveRecord::Base
  validates :license, :ranking, presence: true
  validates :license, :ranking, numericality: { greater_than: 0 }
  validates_uniqueness_of :license, scope: :tournament_id
  validates_uniqueness_of :ranking, scope: :tournament_id
  validates_uniqueness_of :user_id, scope: :tournament_id
  has_many :matches
end
