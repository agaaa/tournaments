class TournamentSponsor < ActiveRecord::Base
	belongs_to :tournament
	belongs_to :sponsor
end
