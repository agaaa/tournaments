class Tournament < ActiveRecord::Base
  validates :name, :location_id, :time, :deadline, presence: true
  validates :limit, numericality: { greater_than: 0 }
  validates :seeded_player, numericality: { greater_than_or_equal_to: 0 }
  validates :name, uniqueness: true
  validate :deadline_not_after_time
  validate :seeded_smaller_then_limit
  validate :time_in_future
  validate :deadline_in_future

  belongs_to :promoter
  has_many :rounds
  has_many :participant
  has_one :location

  private

  def deadline_not_after_time
    unless deadline != nil and time != nil and deadline <= time
      errors.add(:deadline, "cannot be after the tournament") 
    end
  end 

  def seeded_smaller_then_limit 
    unless seeded_player != nil and limit != nil && seeded_player <= limit/2
      errors.add(:seeded, "must be smaller then limit of players") 
    end
  end 

  def time_in_future 
    unless time != nil and time > (Time.now + 2*60*60)
      errors.add(:time, "must refer to the future") 
    end
  end

  def deadline_in_future 
    unless deadline != nil and deadline > (Time.now + 2*60*60)
      errors.add(:deadline, "must refer to the future") 
    end 
  end

end
